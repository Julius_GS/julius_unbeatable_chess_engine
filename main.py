import numpy as np
class Board:
    def __init__(self):
        self.board = [0]*64
        self.turn = "w"
    def move_piece(self,sq1,sq2):
        self.board[sq2] = self.board[sq1]
        self.board[sq1] = 0
        self.turn = {"w","b"}.remove(self.turn).pop()
    def print_board(self):
        print(np.array(self.board).reshape((8,8))
        # give it an 8x8 shape and call the pieces nice names, like
        # rnbqkbnr
        # pppppppp
        # 00000000
        # 00000000
        # 00000000
        # 00000000
        # PPPPPPPP
        # RNBQKBNR
    def read_PEG(self):
        pass
chess = Board()
chess.print_board()
# do i need a visualizer? how do i store the board? i store the
# board by assigning every piece a number and storing that on an
# 8x8 grid.
# just getting all allowed moves is already really hard.
