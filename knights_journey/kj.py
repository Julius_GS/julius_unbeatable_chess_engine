import numpy as np
import copy
class Board:
    def __init__(self,n):
        self.counter = 0
        self.board = np.zeros((n,n),dtype=int)
        self.x=0
        self.y=0
        self.curmove = 1
        self.board[0][0]=1
        self.solved = False
        self.size = n
    def get_nb(self,x,y):
        knightmoves = {(x,y) for x in [-2,-1,1,2] for y in [-2,-1,1,2] if abs(x)!=abs(y)}
        squares = {(x,y) for x in range(self.size) for y in range(self.size)}
        moves = {(x+xi,y+yi) for (xi,yi) in knightmoves}
        return moves.intersection(squares)
    def am_solved(self):
        for x in range(self.size):
            for y in range(self.size):
                if self.board[x][y] == 0:
                    return False
        return True
    def get_moves(self,x,y):
        moves = self.get_nb(x,y)
        return [(x,y) for (x,y) in moves if self.board[x][y] == 0]
    def gen_move_list(self):
        sol = board.solved
        returngrid = []
        for i in range(1,self.size**2+1):
            returngrid.append(np.argwhere(sol == i).tolist()[0])
        return returngrid
    def solve(self):
        if self.counter > 0:
            return
        if self.am_solved():
            self.counter += 1
            print("SOLVED")
            print(self.board)
            self.solved = copy.deepcopy(self.board)
            return True
        oldx = self.x
        oldy = self.y
        movelist = self.get_moves(self.x,self.y)
        # now we sort this to get the ones with the fewest
        # opportunities first!
        movelist = sorted(movelist,key=lambda x:len(self.get_moves(x[0],x[1])))
        for av_sq in movelist: # available square
            self.curmove += 1
            (self.x,self.y)=av_sq
            self.board[self.x][self.y] = self.curmove
            self.solve()
            self.board[self.x][self.y] = 0
            self.x = oldx
            self.y = oldy
            self.curmove -= 1
        return False

board = Board(6)
board.solve()
board.gen_move_list()

# challenge: write a closed journey!
